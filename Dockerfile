FROM nginx:1.16

# Change directories owwnership to start nginx as non root
RUN chown -R nginx /var/cache/nginx /var/log/nginx;

COPY nginx.conf /etc/nginx/
COPY default.conf /etc/nginx/conf.d/

COPY ./WebSites /usr/share/nginx/web/

USER nginx
